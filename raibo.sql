-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 10:42 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raibo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_question` varchar(200) NOT NULL,
  `asked_at` varchar(200) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_question`, `asked_at`, `speaker`, `answered`) VALUES
(2, 'unknown', 'Hi\r\n', '2020/12/03 12:39:34', 0, 0),
(3, 'unknown', 'hi, this looks chic\r\ncan we not remove \'this event hasn\'t started yet\'? ', '2020/12/04 15:37:26', 0, 0),
(4, 'unknown', 'can we change the music? ', '2020/12/04 15:42:40', 0, 0),
(5, 'unknown', 'i can send something that is queer-centric', '2020/12/04 15:42:53', 0, 0),
(6, 'unknown', 'do we need to up the volume a bit? ', '2020/12/04 15:44:10', 0, 0),
(7, 'unknown', 'folks - this looks real cool', '2020/12/04 15:45:04', 0, 0),
(8, 'unknown', 'just one problem - it is pop culture - two words\r\n', '2020/12/04 15:45:29', 0, 0),
(9, 'unknown', 'can we un-list the video - just got an email alert that we are going live ', '2020/12/04 15:46:55', 0, 0),
(10, 'unknown', 'What\'s so amazing about this fest ?', '2020/12/04 19:11:52', 0, 0),
(11, 'unknown', 'When will rlf start ', '2020/12/05 09:48:01', 0, 0),
(12, 'unknown', 'Super excited. Many Congratulations Sharif â¤ï¸', '2020/12/05 10:01:04', 0, 0),
(13, 'unknown', 'Will ask later', '2020/12/05 10:03:02', 0, 0),
(14, 'unknown', '.', '2020/12/05 10:11:01', 0, 0),
(15, 'unknown', 'Good wishes and loads of love to Digital & One! What a wonderful start...enjoying this conversation! Looking forward to all the awesomeness coming our way today and tomorrow. Excited! \r\n-Vishal Ghatge', '2020/12/05 10:16:41', 0, 0),
(16, 'unknown', 'Sashi, you\'re very famously focused on your contituency, visiting it many times a year. Could you do something for us there;, which might grab attention nationwide and emulated elsewhere?\r\n ', '2020/12/05 10:30:12', 0, 0),
(17, 'unknown', 'Why do we need to justify our queerness as per \'Hinduism\' anyway? And why hasn\'t even Congress been pro LGBTQ? ', '2020/12/05 10:33:15', 0, 0),
(18, 'unknown', 'When do you think the Civil Union  will be recognized in India, as that\'s the next step we look forward to post 377.', '2020/12/05 10:44:32', 0, 0),
(19, 'unknown', 'Lots of respect to y\'all for all the hardships you guys have been through but I wanted to ask that what was the one good thing that you know, you had throughout your journey? ', '2020/12/05 10:52:07', 0, 0),
(20, 'unknown', 'Navin, I still remember it like yesterday! You said \'I am Catholic and I am Gay\'! We all were stunned and so happy to see you on stage!! Love, love.\r\n', '2020/12/05 10:52:42', 0, 0),
(21, 'unknown', 'Please help him with an applaud please. #respect.\r\n', '2020/12/05 11:12:39', 0, 0),
(22, 'unknown', 'Sushant is definitely one of the people who uses his platform to support queer people. I once tagged him in a crisis, he immediately replied', '2020/12/05 11:14:56', 0, 0),
(23, 'unknown', 'With OTT coming under censorship too, would queer cinema stand a chance in India? What do you think is the future of queer cinema in that regard?', '2020/12/05 11:22:55', 0, 0),
(24, 'unknown', 'it was a wonderful discussion - sukhi', '2020/12/05 11:25:39', 0, 0),
(25, 'unknown', 'What are your thoughts on content like Deepa Mehta\'s. Do you think it\'s good that she\'s foregrounding \"queer\" narratives or do you think that she\'s taking up queer spaces?', '2020/12/05 11:29:05', 0, 0),
(26, 'unknown', 'Enjoyed every bit of this conversation!\r\n-Vishal Ghatge', '2020/12/05 11:34:55', 0, 0),
(27, 'unknown', 'Maya you are an archive as well! :)', '2020/12/05 11:45:30', 0, 0),
(28, 'unknown', 'Where can we find the report of the Bangalore queer archiving workshop? And are there any more similar workshops that you will host in the future? How do we continue to archive?', '2020/12/05 11:53:47', 0, 0),
(29, 'unknown', 'Is there any plan of how the data need to be made public of the archive Qamra in the future? ', '2020/12/05 12:23:50', 0, 0),
(30, 'unknown', 'Could I be directed to the kamra website?\r\n', '2020/12/05 12:30:14', 0, 0),
(31, 'unknown', 'Qamra can provide opportunities to volunteers to contribute towards Qamra via various internships. ', '2020/12/05 12:31:33', 0, 0),
(32, 'unknown', 'I\'m so excited!!!', '2020/12/05 12:35:31', 0, 0),
(33, 'unknown', 'On top of what Daniel mentioned, even A in the LGBTQIA is considered \'Ally\' and aro/ace spectrum identities are also erased', '2020/12/05 12:54:47', 0, 0),
(34, 'unknown', 'Thank you for bringing in so many perspectives about invisibility within the queer community. I was wondering how we can make intersectionality a staple in conversations about the LGBTQIA+ community?', '2020/12/05 13:00:15', 0, 0),
(35, 'unknown', 'Daniel, Did your parents encounter the Hijra community during your childhood? Or were you forced to join the Hijra community anytime? ', '2020/12/05 13:00:39', 0, 0),
(36, 'unknown', 'Could the speakers share a little bit about their thoughts on inspiration porn?', '2020/12/05 13:06:45', 0, 0),
(37, 'unknown', 'I had so many more questions ðŸ˜­â¤ï¸â¤ï¸â¤ï¸â¤ï¸ thank you for doing this everyone!!!', '2020/12/05 13:30:16', 0, 0),
(38, 'unknown', 'Wow! So simple. ðŸ‘', '2020/12/05 13:39:36', 0, 0),
(39, 'unknown', 'Sakshi, is there any way to reach you for someone trying to set up an org in a simiar sustaible way as gaysi?', '2020/12/05 13:58:22', 0, 0),
(40, 'unknown', 'If the Kama Sutra beyond \"sex guide\" why has it really been reduced to a sex guide by people right now?\r\n\r\n', '2020/12/05 16:30:27', 0, 0),
(41, 'unknown', 'As a trans woman, I think the biggest thing I want to add is the biggest difference is going against the grain as it were', '2020/12/05 16:58:19', 0, 0),
(42, 'unknown', 'I wish you would update the schedule on your website. It is from 2019. Can\'t find the new one anywhere. Very inconvenient', '2020/12/05 18:08:16', 0, 0),
(43, 'unknown', 'Enjoyed every bit of today. Particularly plurality panel, loved it. Looking forward to panel screening and of course the great lineup tomorrow. Cheers! -Vishal Ghatge', '2020/12/05 19:35:19', 0, 0),
(44, 'unknown', 'Sharif and Shivraj, you guys are aweosme! Cheers to everyone involved to put together Digital and One despite all the challenges. Kudos to you all. - Vishal Ghatge ', '2020/12/05 19:37:20', 0, 0),
(45, 'unknown', 'What are the day to day Challenges you face Mahi? ', '2020/12/05 19:39:18', 0, 0),
(46, 'unknown', 'Mahi what challenges do you encounter as a working Transwomen in Corporate sector? ', '2020/12/05 20:08:01', 0, 0),
(47, 'unknown', 'Questions to Mahi - What was the hardest decision she ever had to make? and What is her greatest career strength?', '2020/12/05 20:11:31', 0, 0),
(48, 'unknown', 'Maahi, how do you look up to people who don\'t belong to your community. I mean you are such an inspiration, I would love to be friends with an inspiration like you.', '2020/12/05 20:12:42', 0, 0),
(49, 'unknown', 'Such a powerful, insightful conversation - @Sumit, @Abhina, @Mahi :)', '2020/12/05 20:22:17', 0, 0),
(50, 'unknown', 'Does Fire not fall into representation by people who don\'t understand what they\'re depicting? How do you balance that with the acclaim it has received?', '2020/12/06 10:48:56', 0, 0),
(51, 'unknown', ' Just wanted to ask you that did you ever have second doubts? About everything? About being a filmmaker and or about being a filmmaker to cast queer characters? ', '2020/12/06 11:06:03', 0, 0),
(52, 'unknown', 'Do some abusive vulgar series like Sacred Games, Mirzapur etc misleading youth and generating steorotypes in youths mind ?', '2020/12/06 11:06:10', 0, 0),
(53, 'unknown', 'How can we stop this Misleading of youth against our LGBTQ perspective as nowadays abusive, ALPHA HETERO series are more toxic.. how can we create our own space?', '2020/12/06 11:07:33', 0, 0),
(54, 'unknown', 'As someone who lives in the US - When and where can one watch sheer qorma?\r\n', '2020/12/06 11:08:45', 0, 0),
(55, 'unknown', 'Can you give an example on how storytelling changes when the story is by a queer person? ', '2020/12/06 11:27:01', 0, 0),
(56, 'unknown', 'Queer characters are often presented as sources of humour. Does humour do queer representation a disservice? Or is there space to negotiate with it towards more positive outcomes?', '2020/12/06 11:28:51', 0, 0),
(57, 'unknown', 'But doesn\'t capitalism encourage a culture of individual aggrandizement which just pushes us, wherever we come from to just hold on to that proverbial mic?', '2020/12/06 12:01:53', 0, 0),
(58, 'unknown', 'Do you think the pluralization of a movement dilutes its intensity, or does it strengthen it? How do you feel it has gone with the queer movement in India?', '2020/12/06 12:01:59', 0, 0),
(59, 'unknown', 'Doesn\'t social media deepen that insecurity and that behaviour?', '2020/12/06 12:02:00', 0, 0),
(60, 'unknown', 'What would be the equivalent of \'equity vs equality\' paradigm in this context?', '2020/12/06 12:03:53', 0, 0),
(61, 'unknown', 'Isn\'t intersectionality a little too used and abused by those in positions of power today?', '2020/12/06 12:04:43', 0, 0),
(62, 'unknown', 'When we look at Indian politics and the rise of OBCs in Bihar and UP, what lessons does it offer in terms of the elite within the marginalized capturing power and not wanting to let go?', '2020/12/06 12:07:43', 0, 0),
(63, 'unknown', 'Can the individual really represent a community, after all, once the person rises to power, what is their incentive to really give back?', '2020/12/06 12:11:29', 0, 0),
(64, 'unknown', 'Cannot log on. ', '2020/12/06 14:12:39', 0, 0),
(65, 'unknown', 'How does this work', '2020/12/06 15:00:17', 0, 0),
(66, 'unknown', 'So true, Saikat...how do you appreciate white without the black...there is also the fear of losing the value of a home altogether too...', '2020/12/06 15:53:34', 0, 0),
(67, 'unknown', 'It\'s very touching, Chitra ma\'am. Proud of you. Motivated to come out. \r\n', '2020/12/06 16:31:38', 0, 0),
(68, 'unknown', 'Chitra mam said they have a plan to create awareness in the regional areas. Please elaborate!', '2020/12/06 16:32:19', 0, 0),
(69, 'unknown', 'Thank you all - awesome session, lovely people. Chitra  you rock! ', '2020/12/06 16:32:24', 0, 0),
(70, 'unknown', 'thank you for this beautiful session', '2020/12/06 16:32:58', 0, 0),
(71, 'unknown', 'This conversation has been going since 2015 and yet donâ€™t see the change in these leadership teams I.e iBM, google, Goldman Sachs, E&Y', '2020/12/06 16:42:54', 0, 0),
(72, 'unknown', 'Can i hear it?', '2020/12/06 16:43:32', 0, 0),
(73, 'unknown', 'Can we talk about inclusive companies that are not family owned..letâ€™s talk abt corporate India/Global Comapnies', '2020/12/06 16:55:05', 0, 0),
(74, 'unknown', 'What is the source of this 200B? Cant see how Fortune 500 wouldnâ€™t jump on this staggering number. ', '2020/12/06 16:58:52', 0, 0),
(75, 'unknown', 'Great points and rationale to understanding this in a workplace  by Ritu and Priya', '2020/12/06 17:06:00', 0, 0),
(76, 'unknown', 'What\'s your experience with inclusion in Indian organisations for Trans people? Are there differences in the working experience here for Transmxn vs. Transwomxn? ', '2020/12/06 17:12:46', 0, 0),
(77, 'unknown', 'Yz it not audible', '2020/12/06 17:13:16', 0, 0),
(78, 'unknown', 'Yz it not audible', '2020/12/06 17:13:50', 0, 0),
(79, 'unknown', 'Yz it not audible', '2020/12/06 17:13:51', 0, 0),
(80, 'unknown', 'Yz it not audible', '2020/12/06 17:14:09', 0, 0),
(81, 'unknown', 'How do you build visible and vocal allies to the LGBTQ+ community at workplaces? Can you share a few examples?', '2020/12/06 17:21:38', 0, 0),
(82, 'unknown', 'cant i see from beginning', '2020/12/06 19:01:17', 0, 0),
(83, 'unknown', 'After gay marriage, will the divorced gays get alimony? Will the gay marriage law mirror the hetero marriage laws? ', '2020/12/06 19:29:56', 0, 0),
(84, 'unknown', 'how effective are anti-discrimination laws going to be? If same sex marriage will not help tackle homophobia, can\'t we ask the same question about anti-discrimination laws as well?\r\n', '2020/12/06 19:31:23', 0, 0),
(85, 'unknown', 'Can\'t wait to see Navin\'s Stand-up ðŸ¤©ðŸ¤©', '2020/12/06 20:28:30', 0, 0),
(86, 'unknown', 'Navin Live!!\r\n', '2020/12/06 20:41:33', 0, 0),
(87, 'unknown', 'Hahaha ', '2020/12/06 20:42:24', 0, 0),
(88, 'unknown', 'Haha', '2020/12/06 20:42:48', 0, 0),
(89, 'unknown', ':)', '2020/12/06 20:42:57', 0, 0),
(90, 'unknown', 'ðŸ¤£ðŸ¤£ðŸ¤£', '2020/12/06 20:43:18', 0, 0),
(91, 'unknown', 'Hahahahshshahahahahahah', '2020/12/06 20:43:53', 0, 0),
(92, 'unknown', 'ðŸ¤£ðŸ¤£ðŸ¤£', '2020/12/06 20:43:54', 0, 0),
(93, 'unknown', 'I can\'t stop laughing on that joke ', '2020/12/06 20:44:06', 0, 0),
(94, 'unknown', 'LOL', '2020/12/06 20:45:09', 0, 0),
(95, 'unknown', 'lol ðŸ˜† ðŸ¤£ ', '2020/12/06 20:45:12', 0, 0),
(96, 'unknown', 'Mom joke still gay\r\nðŸ¤£ðŸ¤£ðŸ¤£ðŸ¤£ðŸ¤£', '2020/12/06 20:45:20', 0, 0),
(97, 'unknown', 'Navin Karona...... hahahah nice one', '2020/12/06 20:45:39', 0, 0),
(98, 'unknown', ':)\r\n', '2020/12/06 20:45:41', 0, 0),
(99, 'unknown', 'ðŸ˜†ðŸ˜†ðŸ˜†', '2020/12/06 20:46:34', 0, 0),
(100, 'unknown', 'Hahaha', '2020/12/06 20:46:52', 0, 0),
(101, 'unknown', ':)', '2020/12/06 20:48:40', 0, 0),
(102, 'unknown', 'i am laughing so hard.\r\n', '2020/12/06 20:48:54', 0, 0),
(103, 'unknown', 'What an incredible event, kudos to the entire team of Rainbow Lit Fest. Loved every bit of it. I\'m sure we all will be celebrate queer literature next year at Gulmohor park.\r\nVishal Ghatge ', '2020/12/06 21:08:33', 0, 0),
(104, 'unknown', 'Thanks Sharif!', '2020/12/06 21:09:12', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
