
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast </title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<!-- Pixel Code for https://datastudio.co.in/ -->
<script async src="https://datastudio.co.in/pixel/Jl29IK7aBIQAdk5i"></script>
<!-- END Pixel Code -->

</head>

<body>



  <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-fluid"></a>
 


<div class="container-fluid">
    <div class="row video-panel mt-2 mb-2 text-center">
        <div class="col-12 col-lg-7 offset-lg-3 mb-1 ">
            <div class="embed-responsive embed-responsive-16by9">
			<iframe src="https://vimeo.com/event/529764/embed/09f34bc00c" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </div>    
        </div>
		 <div class="col-12 col-md-7 mt-lg-5 offset-lg-3">
          <div id="question" class="mb-1">
              <div id="question-form" class="panel panel-default">
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" rows='3' cols='2' required placeholder="Please ask your question" ></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?= 'unknown'; ?>">
						  
                          <button class="btn btn-primary btn-sm" type="submit">Submit your Question</button>
                          </div>
                      </div>
                </form>
              </div>
          </div>
          
          
        </div>
        
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
    
	$(document).on('submit', '#question-form form', function()
    {  
	
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});
</script>

</body>

</html>